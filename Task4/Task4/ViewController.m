//
//  ViewController.m
//  Task4
//
//  Created by 陈志坚 on 2021/9/9.
//

#import "ViewController.h"
#import "Masonry.h"
#import "DayTimeView.h"
#import "NightView.h"
#import "DayTimeButton.h"
#import "NightButton.h"
#import "AddButton.h"


#define ScreenWidth     [[UIScreen mainScreen] bounds].size.width
#define ScreenHeight    [[UIScreen mainScreen] bounds].size.height

@interface ViewController ()

@property (nonatomic, strong) AddButton *addButton;
@property (nonatomic, strong) NightView *nightView;
@property (nonatomic, strong) DayTimeView *dayTimeView;
@property (nonatomic, strong) NightButton *nightButton;
@property (nonatomic, strong) DayTimeButton *dayTimeButton;

@property (nonatomic, assign) BOOL isOpen;
@property (nonatomic, assign) BOOL isDayTime;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    //初始化标志位
    self.isOpen = NO;
    self.isDayTime = YES;
    //sekf.addButton init
    self.addButton = [[AddButton alloc] init];
    self.addButton.backgroundColor = UIColor.clearColor;
    [self.addButton addTarget:self action:@selector(addBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.addButton];
    [self.addButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(@55);
        make.height.equalTo(@55);
        make.centerX.equalTo(self.view);
        make.bottom.equalTo(@-100);
    }];
    //self.dayTimeView init
    self.dayTimeView =[[DayTimeView alloc] init];
    self.dayTimeView.backgroundColor = UIColor.clearColor;
    [self.view addSubview:self.dayTimeView];
    [self.dayTimeView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.height.equalTo(@120);
//        make.top.equalTo(@100);
        make.top.equalTo(@(ScreenHeight / 2 - ScreenWidth * 0.8 + 40));
        make.centerX.equalTo(self.view);
    }];
    NSLog(@"%f", ScreenHeight / 2 - ScreenWidth * 0.7);
    //self.nightView init
    self.nightView = [[NightView alloc] init];
    self.nightView.backgroundColor = UIColor.clearColor;
    [self.view addSubview:self.nightView];
    [self.nightView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.view.mas_left);
        make.top.equalTo(@100);
        make.width.height.equalTo(@120);
    }];
    //self.nightButton init
    self.nightButton = [[NightButton alloc] init];
    self.nightButton.hidden = YES;
    self.nightButton.alpha = 0;
    self.nightButton.backgroundColor = UIColor.clearColor;
    [self.nightButton addTarget:self action:@selector(nightBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.nightButton];
    [self.nightButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.addButton);
        make.height.equalTo(@50);
        make.width.equalTo(@50);
        self.nightButton.transform = CGAffineTransformMakeScale(0.2, 0.2);
        make.left.equalTo(@([UIScreen mainScreen].bounds.size.width/2 - 10));
    }];
    //self.dayTimeButton init
    self.dayTimeButton = [[DayTimeButton alloc] init];
    self.dayTimeButton.hidden = YES;
    self.dayTimeButton.alpha = 0;
    self.dayTimeButton.backgroundColor = UIColor.clearColor;
    [self.dayTimeButton addTarget:self action:@selector(dayTimebtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.dayTimeButton];
    [self.dayTimeButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.addButton);
        make.height.equalTo(@50);
        make.width.equalTo(@50);
        self.nightButton.transform = CGAffineTransformMakeScale(0.2, 0.2);
        make.right.equalTo(@(-[UIScreen mainScreen].bounds.size.width/2 - 10));
    }];
}

- (void)addBtnClick:(AddButton *)btn {
    if (self.isOpen) {
        [UIView animateWithDuration:0.5 animations:^{
            self.addButton.transform = CGAffineTransformMakeRotation(0);
            
            self.nightButton.hidden = NO;
            [self.nightButton mas_updateConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(@([UIScreen mainScreen].bounds.size.width/2 - 10));
            }];
            self.nightButton.transform = CGAffineTransformMakeScale(0.2, 0.2);
            self.nightButton.alpha = 0;
            
            self.dayTimeButton.hidden = NO;
            [self.dayTimeButton mas_updateConstraints:^(MASConstraintMaker *make) {
                make.right.equalTo(@(-[UIScreen mainScreen].bounds.size.width/2 - 10));
            }];
            self.dayTimeButton.transform = CGAffineTransformMakeScale(0.2, 0.2);
            self.dayTimeButton.alpha = 0;
            
            [self.view layoutIfNeeded];
        }];
        self.isOpen = NO;
        NSLog(@"关闭");
        
    } else {
        [UIView animateWithDuration:0.5 animations:^{
            self.addButton.transform = CGAffineTransformMakeRotation(M_PI * 0.5);
            
            self.nightButton.hidden = NO;
            [self.nightButton mas_updateConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(@50);
            }];
            self.nightButton.transform = CGAffineTransformMakeScale(1, 1);
            self.nightButton.alpha = 1;
            
            self.dayTimeButton.hidden = NO;
            [self.dayTimeButton mas_updateConstraints:^(MASConstraintMaker *make) {
                make.right.equalTo(@-50);
            }];
            self.dayTimeButton.transform = CGAffineTransformMakeScale(1, 1);
            self.dayTimeButton.alpha = 1;
            
            [self.view layoutIfNeeded];
        } completion:^(BOOL finished) {
            
        }];
        self.isOpen = YES;
    }
}

- (void)nightBtnClick:(NightButton *)btn {
    if (self.isDayTime) {
        [UIView animateWithDuration:0.5 animations:^{
            self.view.backgroundColor = [UIColor colorWithRed:128.0/255 green:128.0/255 blue:128.0/255 alpha:1];
        }];
        
//        CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"backgroundColor"];
//        animation.toValue = (__bridge id _Nullable)([UIColor colorWithRed:128.0/255 green:128.0/255 blue:128.0/255 alpha:1].CGColor);
//        animation.duration = 0.5f;
//        animation.fillMode = kCAFillModeForwards;
//        animation.removedOnCompletion = NO;
//        [self.view.layer addAnimation:animation forKey:@"backgroundColor"];
        //太阳下去
        CAKeyframeAnimation *keyAnimation1 = [CAKeyframeAnimation animationWithKeyPath:@"position"];
        UIBezierPath *path1 = [UIBezierPath bezierPathWithArcCenter:CGPointMake(ScreenWidth/2, ScreenHeight/2 + 100) radius:ScreenWidth*0.8 startAngle:-M_PI * 0.5 endAngle:0 clockwise:1];
        keyAnimation1.path = path1.CGPath;
        keyAnimation1.duration = 0.5;
        keyAnimation1.fillMode = kCAFillModeForwards;
        keyAnimation1.removedOnCompletion = NO;
        [self.dayTimeView.layer addAnimation:keyAnimation1 forKey:@"position"];
        //月亮出来
        CAKeyframeAnimation *keyAnimation2 = [CAKeyframeAnimation animationWithKeyPath:@"position"];
        UIBezierPath *path2 = [UIBezierPath bezierPathWithArcCenter:CGPointMake(ScreenWidth/2, ScreenHeight/2 + 100) radius:ScreenWidth*0.8 startAngle:-M_PI endAngle:-M_PI*0.5 clockwise:1];
        keyAnimation2.path = path2.CGPath;
        keyAnimation2.beginTime = CACurrentMediaTime() + 0.5;
        keyAnimation2.duration = 0.5;
        keyAnimation2.fillMode = kCAFillModeForwards;
        keyAnimation2.removedOnCompletion = NO;
        [self.nightView.layer addAnimation:keyAnimation2 forKey:@"position"];
        //self.dayTimeView位置变到不在屏幕内
        [self.dayTimeView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.view.mas_left);
            make.top.equalTo(@100);
            make.width.height.equalTo(@120);
        }];
        [self.view layoutIfNeeded];
        self.isDayTime = NO;
    }
}

- (void)dayTimebtnClick:(DayTimeButton *)btn {
    if (!self.isDayTime) {
        //改变背景
        [UIView animateWithDuration:0.5 animations:^{
            self.view.backgroundColor = UIColor.whiteColor;
        }];
        //月亮下去
        CAKeyframeAnimation *keyAnimation1 = [CAKeyframeAnimation animationWithKeyPath:@"position"];
        UIBezierPath *path1 = [UIBezierPath bezierPathWithArcCenter:CGPointMake(ScreenWidth/2, ScreenHeight/2 + 100) radius:ScreenWidth*0.8 startAngle:-M_PI * 0.5 endAngle:0 clockwise:1];
        keyAnimation1.path = path1.CGPath;
        keyAnimation1.duration = 0.5;
        keyAnimation1.fillMode = kCAFillModeForwards;
        keyAnimation1.removedOnCompletion = NO;
        [self.nightView.layer addAnimation:keyAnimation1 forKey:@"position"];
        //太阳出来
//        self.nightView.hidden = NO;
        CAKeyframeAnimation *keyAnimation2 = [CAKeyframeAnimation animationWithKeyPath:@"position"];
        UIBezierPath *path2 = [UIBezierPath bezierPathWithArcCenter:CGPointMake(ScreenWidth/2, ScreenHeight/2 + 100) radius:ScreenWidth*0.8 startAngle:-M_PI endAngle:-M_PI*0.5 clockwise:1];
        keyAnimation2.path = path2.CGPath;
        keyAnimation2.beginTime = CACurrentMediaTime() + 0.5;
        keyAnimation2.duration = 0.5;
        keyAnimation2.fillMode = kCAFillModeForwards;
        keyAnimation2.removedOnCompletion = NO;
        [self.dayTimeView.layer addAnimation:keyAnimation2 forKey:@"position"];

        [self.view layoutIfNeeded];
        self.isDayTime = YES;
    }
}


@end
