//
//  NightButton.m
//  Task4
//
//  Created by 陈志坚 on 2021/9/9.
//

#import "NightButton.h"

@implementation NightButton

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
- (void)drawRect:(CGRect)rect {
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGFloat width = rect.size.width;
    
    [[UIColor clearColor] set];
    CGContextFillRect(context, rect);
    CGContextAddArc(context, width/2, width/2, width/2, 0, M_PI * 2, 0);
    CGContextSetFillColorWithColor(context, [UIColor colorWithRed:128.0/255 green:128.0/255 blue:128.0/255 alpha:1].CGColor);
    CGContextFillPath(context);
    
    CGContextSetLineWidth(context, 1);
    CGContextAddArc(context, width/2, width/2, width/2 - 1, 0, M_PI * 1.5, 0);
    [[UIColor whiteColor] set];
    CGContextStrokePath(context);
    
    CGPoint point1 = CGPointMake(width/2, 0);
    CGPoint point2 = CGPointMake(width, width/2);
    CGContextAddArc(context, (point1.x + point2.x)/2, (point1.y + point2.y)/2, (sqrtf(2.0)*(width/2))/2, 0, M_PI*1.25, 0);
    CGContextStrokePath(context);
}

@end
