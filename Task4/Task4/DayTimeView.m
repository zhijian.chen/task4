//
//  DayTimeView.m
//  Task4
//
//  Created by 陈志坚 on 2021/9/9.
//

#import "DayTimeView.h"

@implementation DayTimeView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void)drawRect:(CGRect)rect {
    //获取上下文
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGFloat width = rect.size.width;
    
    [[UIColor clearColor] set];
    CGContextFillRect(context, rect);
    
    CGContextAddArc(context, width/2, width/2, width/2, 0, M_PI * 2, 0);
    CGContextSetFillColorWithColor(context, UIColor.yellowColor.CGColor);
    CGContextFillPath(context);
}

@end
