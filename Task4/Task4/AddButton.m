//
//  AddButton.m
//  Task4
//
//  Created by 陈志坚 on 2021/9/9.
//

#import "AddButton.h"

@implementation AddButton

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void)drawRect:(CGRect)rect {
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGFloat width = rect.size.width;
    
    [[UIColor clearColor] set];
    CGContextFillRect(context, rect);
    
    //画圆
    CGContextSetLineWidth(context, 3);
    [[UIColor greenColor] set];
    CGContextAddArc(context, width/2, width/2, width/2 - 5, 0, M_PI * 2, 0);
    CGContextStrokePath(context);
    
    //画中间的线
    CGContextSetLineWidth(context, 8);
    CGContextMoveToPoint(context, 4, width/2);
    CGContextAddLineToPoint(context, width - 4, width/2);
    CGContextDrawPath(context, kCGPathFillStroke);
    
    CGContextMoveToPoint(context, width/2, 4);
    CGContextAddLineToPoint(context, width/2, width - 4);
    CGContextDrawPath(context, kCGPathFillStroke);
}

@end
